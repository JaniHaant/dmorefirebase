import 'react-native-gesture-handler';
import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ActivityIndicator,
} from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';

import Activities from './src/screens/activities'
import uusactivity from './src/screens/activityContent'
import editprofileScreen from './src/screens/editprofileScreen';
// import  Card  from './src/components/card'
import signIn from './src/components/signIn'
import Activitycard from './src/screens/activitycard';
import profileSearch from './src/screens/profileSearch';
import addActivivites from './src/screens/addActivities';
import signInEmail from './src/components/signInEmail';
import UserScreen from './src/screens/userScreen';
import landingPage from './src/screens/landingPage';


const Stack = createStackNavigator();

const App = () => {

  return (
      <NavigationContainer>
          <Stack.Navigator  initialRouteName="login" >
            <Stack.Screen  name="login" options={{headerShown: false}} component={landingPage} />
            <Stack.Screen name="editprofile" options={{headerShown: false}} component={editprofileScreen} />
            <Stack.Screen options={{headerShown: false}} name="activities" component={addActivivites} />
            <Stack.Screen options={{headerShown: false}} name="search" component={uusactivity} />
            <Stack.Screen options={{headerShown: false}} name="signup" component={signInEmail} />
            <Stack.Screen options={{headerShown: false}} name="activitycard" component={Activitycard} />
            <Stack.Screen options={{headerShown: false}} name="signin" component={signIn} />
            <Stack.Screen options={{headerShown: false}} name="user" component={UserScreen} />
        </Stack.Navigator>    
      </NavigationContainer>
  )
};



export default App;
