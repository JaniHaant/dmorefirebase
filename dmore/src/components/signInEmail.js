import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Button, TextInput,ImageBackground,TouchableOpacity } from 'react-native';
import { Formik, } from 'formik'
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import MainTabScreen from '../screens/mainTabScreen'
// import { TouchableOpacity } from 'react-native-gesture-handler';

    const createUser = (values) => {
        auth()
        .createUserWithEmailAndPassword(values.email, values.password , values.username, values.bio, values.nationality,)
        .then((user) => {
            firestore()
            .collection('users')
            .add({
              idToken: user.user.uid,
              email: values.email,
              username: values.username,
              bio: values.bio,
              nationality: values.nationality        
        })
            console.log('userdata', user);
        })
        .catch(error => {
            if (error.code === 'auth/email-already-in-use') {
            console.log('That email address is already in use!');
            }

            if (error.code === 'auth/invalid-email') {
            console.log('That email address is invalid!');
            }

            console.error(error);
        });
        console.log(values)
    }

    logoff = () => {
      auth()
        .signOut()
        .then(() => console.log('User signed out!'))
    }

function signInEmail({ navigation }) {
  // Set an initializing state whilst Firebase connects
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();

  // Handle user state changes
  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (initializing) return null;

  if (!user) {
    return (
      <View style={styles.container}>
        <ImageBackground style={styles.backgroundimg} source={require('./images/landbg.jpeg')}>
          <View style={styles.header}>
              <Text style={styles.headertext}>Sign Up</Text>
          </View>
          <Formik
            initialValues={{ email: '', password: '', username:'', bio: '', nationality: '', }}
            onSubmit={(values) => 
                createUser(values)
            }
            >
            {({ handleChange, handleBlur, handleSubmit, values }) => (
            <View style={styles.formContainer}>
                <TextInput style={styles.form}
                placeholder='Email'
                placeholderTextColor="white"
                onChangeText={handleChange('email')}
                onBlur={handleBlur('email')}
                value={values.email}
                />
                <TextInput style={styles.form}
                placeholder='Password'
                placeholderTextColor="white"
                onChangeText={handleChange('password')}
                onBlur={handleBlur('email')}
                value={values.password}
                secureTextEntry={true}
                />
                <TextInput style={styles.form}
                placeholder='User Name'
                placeholderTextColor="white"
                onChangeText={handleChange('username')}
                onBlur={handleBlur('username')}
                value={values.username}
                
                />
                <TextInput style={styles.form}
                placeholder='bio'
                placeholderTextColor="white"
                onChangeText={handleChange('bio')}
                onBlur={handleBlur('bio')}
                value={values.bio}
                />
                <TextInput style={styles.form}
                placeholder='Nationality'
                placeholderTextColor="white"
                onChangeText={handleChange('nationality')}
                onBlur={handleBlur('nationality')}
                value={values.nationality}
                />
                <View style={styles.sign}>
                  <TouchableOpacity style={styles.button} onPress={handleSubmit}>
                    <Text style={{fontSize: 15, fontWeight: 'bold', color: 'white'}}>Register</Text>
                  </TouchableOpacity>
                  {/* <Button onPress={handleSubmit} title="Sign Up" /> */}
                </View>
            </View>
            )}
        </Formik>
        </ImageBackground>
      </View>
    );
  }

  return (
    <MainTabScreen/>
    
  );
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        
    },
    backgroundimg:{
      flex: 1,
      resizeMode: "cover",
      justifyContent: "center",
      alignItems: 'center'
      
    },
    header:{
        marginTop: 20,
        marginBottom: 50,
        alignItems:'center',
        justifyContent: 'center',
        
    },
    headertext:{
        fontWeight: 'bold',
        fontSize:30,
        fontFamily: 'serif',
        color: 'blue'
    },
    formContainer:{
        alignItems:'center',
        justifyContent:'center',
        marginBottom: 40
        
    },
    form:{
        borderWidth: 1,
        marginBottom: 4,
        borderColor: 'black',
        borderRadius: 5,
        width: 250,
        alignItems:'center',
        justifyContent: 'center',
        backgroundColor: '"rgba(145,155,216, 0.8)"'
    },
    login:{
      justifyContent:'center',
      alignItems:'center'
    },
    button:{
      backgroundColor: '"rgba(44,120,164, 0.9)"',
      padding: 10,
      width: 250,
      alignItems: 'center',
      marginBottom: 10,
      borderRadius: 10
  },
    sign:{
      marginTop: 20
    }
})

export default signInEmail



