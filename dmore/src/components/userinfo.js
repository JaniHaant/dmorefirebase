import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'

import firestore from '@react-native-firebase/firestore'

class userinfo extends Component{
    state = {
        userinfo: []
    }
    constructor(props){
        super(props);
        this.subscriber =
            firestore()
            .collection("userinfo")
            .onSnapshot(docs => {
                let userinfo = []
                docs.forEach(doc => {
                    userinfo.push(doc.data())
                })
                this.setState({ userinfo })
                console.log(userinfo)
        })

    }
     
    render() {
        return (
            <View style={styles.container}>
                {this.state.userinfo.map((userinfo, index) => <View key={index}>
          <View style={styles.userInfoSection}> 
            <View style={styles.useInfoContainer}>
              <Text style={{fontSize:18}}>email</Text>
              <Text style={{marginTop:10, marginLeft:5, fontSize: 16}}>{userinfo.name}</Text>
            </View>
            <View style={styles.useInfoContainer}>
              <Text style={{fontSize:18}}>info</Text>
              <Text style={{marginTop:10, marginLeft:5, fontSize: 16}}>{userinfo.bio}</Text>
            </View>
            <View style={styles.useInfoContainer}>
              <Text style={{fontSize:18}}>nationality</Text>
              <Text style={{marginTop:10, marginLeft:5, fontSize: 16}}>{userinfo.nationality}</Text>
            </View>
          </View> 
                        </View>)} 
                </View>   
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        padding: 10,
    },
    header:{
        alignItems:'center',
        justifyContent: 'center',
        marginTop: 5,
    },
    headertext:{
        fontSize:20,
        fontFamily: 'serif',
    },
    inputcontainer:{
        flex:1,
        marginTop:10,

    },
    input:{
        marginTop: 10,
      borderWidth: 1,
      borderColor: '#ddd',
      padding: 10,
      fontSize: 18,
      borderRadius: 6,
    },
    category:{
        justifyContent:'center',
     
    },
    categorytext:{
        fontSize:18,
        paddingLeft:10,
        fontFamily: 'serif',
        marginTop: 10,
        borderBottomWidth:1,
        borderColor: 'black',
    
    },
    picker:{
        
    }

  })

export default userinfo