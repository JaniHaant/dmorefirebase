import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Button, TextInput, ImageBackground } from 'react-native';
import { Formik, } from 'formik'
import auth from '@react-native-firebase/auth';
import MainTabScreen from '../screens/mainTabScreen'

const login = (values) => {
  auth()
  .signInWithEmailAndPassword(values.email, values.password , values.username, values.bio, values.nationality,)
  .then((user) => {
      console.log('userdata', user);
  })
  .catch(error => {
      if (error.code === 'auth/email-already-in-use') {
      console.log('That email address is already in use!');
      }

      if (error.code === 'auth/invalid-email') {
      console.log('That email address is invalid!');
      }

      console.error(error);
  });
}

export default function signIn({ navigation }) {
    const [user, setUser] = useState();
    const [initializing, setInitializing] = useState(true);
    // tästä
     
     // Handle user state changes
    function onAuthStateChanged(user) {
        setUser(user);
        if (initializing) setInitializing(false);
    }
      
    useEffect(() => {
        const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
        return subscriber; // unsubscribe on unmount
    }, []);
      
    if (initializing) return null;
        if (!user) {
          return (
            <View style={styles.container}>
                <ImageBackground style={styles.backgroundimg} source={require('./images/landbg.jpeg')}>
            <View style={styles.header}>
              <Text style={styles.headertext}>Sign Up</Text>
          </View>
        <Formik
            initialValues={{ email: '', password: '', }}
            onSubmit={(values) => 
                login(values)
            }
            >
            {({ handleChange, handleBlur, handleSubmit, values }) => (
            <View style={styles.formContainer}>
                <TextInput style={styles.form}
                placeholder='Email'
                placeholderTextColor="white"
                onChangeText={handleChange('email')}
                onBlur={handleBlur('email')}
                value={values.email}
                />
                <TextInput style={styles.form}
                placeholder='Password'
                placeholderTextColor="white"
                onChangeText={handleChange('password')}
                onBlur={handleBlur('email')}
                value={values.password}
                secureTextEntry={true}
                />
                <Button onPress={handleSubmit} title="Login" />
            </View>
            )}
        </Formik>
        <View>
        </View>
        </ImageBackground>
      </View>
          );
        }
      
        return (
          <MainTabScreen/>
          
        );
      }

    // tähän
    
  

  const styles = StyleSheet.create({
    container:{
        flex: 1,
    },
    backgroundimg:{
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center",
        alignItems: 'center'
        
      },
    formContainer:{
        alignItems:'center',
        justifyContent:'center',
        marginBottom: 40      
    },
    header:{
        marginBottom: 100,
        alignItems:'center',
        justifyContent: 'center',
        
    },
    headertext:{
        fontWeight: 'bold',
        fontSize:30,
        fontFamily: 'serif',
        color: 'blue'
    },
    form:{
        borderWidth: 1,
        marginBottom: 4,
        borderColor: 'black',
        borderRadius: 5,
        width: 250,
        alignItems:'center',
        justifyContent: 'center',
        backgroundColor: '"rgba(145,155,216, 0.8)"'
    },
    button:{   
      alignItems: 'center',
      justifyContent: 'center'
    }
})