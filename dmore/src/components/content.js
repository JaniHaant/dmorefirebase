import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'

import firestore from '@react-native-firebase/firestore'

class content extends Component{
    state = {
        activities: []
    }
    constructor(props){
        super(props);
        this.subscriber =
            firestore()
            .collection("activities")
            .onSnapshot(docs => {
                let activities = []
                docs.forEach(doc => {
                    activities.push(doc.data())
                })
                this.setState({ activities })
                console.log(activities)
        })

    }
     
    render() {
        return (
            <View style={styles.container}>
                {this.state.activities((activities, index) => <View key={index}>
                <View style={styles.card}>
                    <ImageBackground source={require('../components/images/GroupImg.jpg')} style={styles.cardImage} >
                    <View style={styles.cardInfo}>
                    <Text style={{fontSize:28, marginBottom:10, fontWeight:'bold', color: 'white'}}>name</Text>
                    <Text style={{fontSize:18, marginBottom:10, fontWeight:'bold',color: 'white'}}>{activities.name}</Text>
                    <Text style={{fontSize:18, marginBottom:10, fontWeight:'bold', color: 'white'}}>minimum people of 2</Text>
                    </View>
                    </ImageBackground>
                    <View style={{flexDirection: 'row',margin: 20, alignItems: 'center',justifyContent:'center', width:'100%'}}>
                    <TouchableOpacity style={{borderRadius: 45,backgroundColor:'red',marginLeft: 5,marginRight: 5,width: 120,padding: 25,alignItems: 'center'}}><Text>PASS</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button}><Text>ATTEND</Text></TouchableOpacity>
                    </View>
                </View> 
                        </View>)} 
                </View>   
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        padding: 10,
    },
    header:{
        alignItems:'center',
        justifyContent: 'center',
        marginTop: 5,
    },
    headertext:{
        fontSize:20,
        fontFamily: 'serif',
    },
    inputcontainer:{
        flex:1,
        marginTop:10,

    },
    input:{
        marginTop: 10,
      borderWidth: 1,
      borderColor: '#ddd',
      padding: 10,
      fontSize: 18,
      borderRadius: 6,
    },
    category:{
        justifyContent:'center',
     
    },
    categorytext:{
        fontSize:18,
        paddingLeft:10,
        fontFamily: 'serif',
        marginTop: 10,
        borderBottomWidth:1,
        borderColor: 'black',
    
    },
    picker:{
        
    }

  })

export default content