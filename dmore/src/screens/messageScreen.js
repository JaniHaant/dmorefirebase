import React from 'react';
import {View, Text,StyleSheet, Button, ImageBackground } from 'react-native';

function messageScreen ({navigation}) {
    return(
        <View style={styles.container}>
            <Text>Messages here</Text>                           
        </View>
    );
} ;

const styles = StyleSheet.create({
    container:{
        flex:1
    },
    background:{
        flex: 1,
        resizeMode: 'cover',  
        width: '100%',
        height:'100%',
    }

})

export default messageScreen