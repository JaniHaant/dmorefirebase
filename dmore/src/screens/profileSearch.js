import React, {useState, useEffect } from 'react'
import {View, StyleSheet, Text, Image, FlatList,TouchableOpacity  } from 'react-native'
import firestore from '@react-native-firebase/firestore';

//select an image that demonstrates your post or activity

export default function profileSearch({ navigation }){

    const [user, setUser] = useState([])
      useEffect(() => {
        firestore()
        .collection('users')
        .onSnapshot(docs => {
          docs.forEach(doc => {
            user.push(doc.data())
            })   
          setUser(user)   
    })     
    }, []
    );

  const Item = ({ item, onPress, style, id }) => (
    <TouchableOpacity onPress={() => navigation.navigate("user")} key={id} onPress={onPress} style={[styles.item, style]} >
      <Text style={styles.title}>{item.username}</Text>
    </TouchableOpacity>
  );

  const renderItem = ({ item }) => {
    return (
      <Item
        item={item}
      />
    );
  };

    console.log(user)
    return(
        <View style={styles.container}>        
           <FlatList
            data={user}
            renderItem={renderItem}
           />
        </View>
        )
}

const styles = StyleSheet.create({
    container:{
      flex: 1,
      padding: 10
    },
    item: {
      padding: 20,
      marginVertical: 8,
      marginHorizontal: 16,
    },
    title: {
      fontSize: 10,
    },
  })