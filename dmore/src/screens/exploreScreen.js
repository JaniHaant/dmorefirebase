import React, { Component } from 'react';
import {View, Text, StyleSheet, Button, TouchableOpacity } from 'react-native';
import MapView, { PROVIDER_GOOGLE, } from 'react-native-maps';
import GetLocation from 'react-native-get-location';
import RNGooglePlaces from 'react-native-google-places';



class exploreScreen extends Component {
    
    openSearchModal() {
        RNGooglePlaces.openAutocompleteModal()
        .then((place) => {
            console.log(place);
            // place represents user's selection from the
            // suggestions and it is a simplified Google Place object.
        })
        .catch(error => console.log(error.message));  // error is a Javascript Error object
      }

    state = {
        location: null,
        loading: false,
    }

    _requestLocation = () => {
        this.setState({ loading: true, location: null });

        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 150000,
        })
            .then(location => {
                this.setState({
                    location,
                    loading: false,
                });
            })
            .catch(ex => {
                const { code, message } = ex;
                console.warn(code, message);
                if (code === 'CANCELLED') {
                    Alert.alert('Location cancelled by user or by another request');
                }
                if (code === 'UNAVAILABLE') {
                    Alert.alert('Location service is disabled or unavailable');
                }
                if (code === 'TIMEOUT') {
                    Alert.alert('Location request timed out');
                }
                if (code === 'UNAUTHORIZED') {
                    Alert.alert('Authorization denied');
                }
                this.setState({
                    location: null,
                    loading: false,
                });
                let initialPosition ={
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    latitudeDelta: 0.015,
                    longitudeDelta: 0.0121
                }
                this.setState({initialPosition})
            });
    }

    render(){ 
        const { location, loading } = this.state;
        return(
        <View style={styles.container}>
            <TouchableOpacity
                style={styles.button}
                onPress={() => this.openSearchModal()}
                >
                <Text>Pick a Place</Text>
            </TouchableOpacity>
            <MapView
                showsUserLocation={true}
                ref={map => this._map = map}
                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                style={styles.map}
                initialRegion={this.state.initialPosition}
        >
     </MapView>
        </View>
    );
}} ;

const styles = StyleSheet.create({
    container: {
      ...StyleSheet.absoluteFillObject,
    //   marginTop:'20%',
    //   height: '90%',
    //   width: 400,
    //   justifyContent: 'flex-end',
      alignItems: 'center',
    },
    map: {
      ...StyleSheet.absoluteFillObject,
      height: '90%',
      marginTop: '10%'

    },
   });
   
export default exploreScreen