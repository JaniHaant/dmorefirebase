import React from 'react';

import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import HomeScreen from './homeScreen'
import ProfileScreen from './profileScreen'
import ExploreScreen from './exploreScreen'
import MessageScreen from './messageScreen'

const HomeStack = createStackNavigator();
const ProfileStack = createStackNavigator();
const ExploreStack = createStackNavigator();
const MessageStack = createStackNavigator();

const Tab = createMaterialBottomTabNavigator();


const MainTabScreen = () => (
    
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: '#fff',
      }}
    >
      <Tab.Screen
        name="Home"
        component={HomeStackScreen}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="home" color={color} size={25} />
          ),
        }}
      />
      <Tab.Screen
        name="Explore"
        component={ExploreStackScreen}
        options={{
            headerShown: false,
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="alarm" color={color} size={25} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileStackScreen}
        options={{
          
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account" color={color} size={25} />
          ),
        }}
      />
      <Tab.Screen
        name="Messages"
        component={MessageStackScreen}
        options={{
          
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="chat" color={color} size={25} />
          ),
        }}
      />
    </Tab.Navigator>
    
)

export default MainTabScreen

const HomeStackScreen = ({navigation}) => (
    <HomeStack.Navigator screenOptions={{
        headerStyle: {
          backgroundColor: '#00bfff',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold'
        },
      }}>
        <HomeStack.Screen name="Home" options={{headerShown: false}} component={HomeScreen}/>
      </HomeStack.Navigator>
)

const ProfileStackScreen = ({navigation}) => (
<ProfileStack.Navigator screenOptions={{
    headerStyle: {
      backgroundColor: '#00bfff',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold'
    },
  }}>
    <ProfileStack.Screen name="Profile" options={{headerShown: false}} component={ProfileScreen} />
  </ProfileStack.Navigator>
  
)
const ExploreStackScreen = ({navigation}) => (
  <ExploreStack.Navigator screenOptions={{
      headerStyle: {
        backgroundColor: '#00bfff',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold'
      },
    }}>
      <ExploreStack.Screen name="Explore" options={{headerShown: false}} component={ExploreScreen} />
    </ExploreStack.Navigator>
    
  )
  const MessageStackScreen = ({navigation}) => (
    <MessageStack.Navigator screenOptions={{
        headerStyle: {
          backgroundColor: '#00bfff',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold'
        },
      }}>
        <MessageStack.Screen name="Message" options={{headerShown: false}} component={MessageScreen} />
      </MessageStack.Navigator>
  
  )
