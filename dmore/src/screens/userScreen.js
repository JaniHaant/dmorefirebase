import React, { useState, useEffect } from 'react';
import {View, Text, StyleSheet, Image, Button } from 'react-native';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { Divider } from 'react-native-paper';
import firestore from '@react-native-firebase/firestore';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';

function userScreen ({navigation}) {
  const [user, setUser] = useState([])

  useEffect(() => {
    firestore()
    .collection('users')
    .doc(CurrentUser.uid)
    .onSnapshot(docs => {
      docs.forEach(doc => {
        user.push(doc.data())
        })   
      setUser(user)   
  })     
  }, []
  );
  console.log(user)
 
    return(  
        <View style={styles.container}>
          <View style={styles.itemcontainer}>       
          <View style={styles.backgroundimg}>
              <View style={styles.cover}>
                <Image source={require('../components/images/koira.jpeg')} style={styles.avatarPlaceholder}></Image>
                <View style={styles.userinfo}>
                </View>
              </View>
          </View>
          <View style={styles.infocontainer}>
            <View style={styles.information}>
              <Text style={{fontSize:24, fontWeight: 'bold'}}>Bio</Text>
              <Text style={{marginLeft:20}}>Tässä on paljon tekstiä joten alkaa tila loppumaanm,Tässä on paljon tekstiä joten alkaa tila loppumaanm</Text>
            </View>
            <Divider style={{color: 'blue', marginTop: 15, marginRight: 10, marginLeft: 10, borderWidth: 0.3}} />
            <View style={styles.information}>
              <Text style={{fontSize:24, fontWeight: 'bold'}}>Nationality</Text>
              <Text style={{marginLeft:20 }}>teksti</Text>
            </View>
            <Divider style={{color: 'blue', marginTop: 15, marginRight: 10, marginLeft: 10, borderWidth: 0.3 }} />
            <View style={{marginTop: 30}}>
            <Button title="Send a message" style={{marginTop: 30}}/>
            </View>
          </View>
          </View>  
        </View>
    );
} ;

const styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor: 'black'
    },
    itemcontainer:{
      flex:1
    },
    top:{
      justifyContent: 'center',
      alignItems:'center'  
    },
    backgroundimg:{
      flex: 0.8,
      resizeMode: "cover",
      justifyContent: "center",
      alignItems: 'center' ,
    },
    cover:{
      backgroundColor: '"rgba(93, 89, 89, 0.7)"',
      width: '100%',
      height: '100%',
      alignItems:'center',
      justifyContent: 'center'
    },
    icon:{
      flexDirection: 'row',
      justifyContent: 'flex-end' ,
      alignItems:'center',
      marginTop:5,
      marginLeft: 280,
      // backgroundColor:'green'
    },
    avatarPlaceholder:{
    flex:1,
    width: '100%',
    height:'100%',
    backgroundColor: '#E1E2E6',
    justifyContent: 'center',
    alignItems: 'center',
    },
    userinfo:{
      // marginTop: 10,
      marginLeft: 20,
    },
    infocontainer:{
      backgroundColor:'white',
      flex:1,
      width:'100%',
      borderRadius: 5
    },
    information:{
      marginLeft: 20,
      marginTop: 30,
      // backgroundColor:'green' 
    },
  ProfileText:{
    marginTop:55,
    marginLeft: 40,
    fontSize: 24
  },
  buttons:{
    marginTop: 10,
    // flexDirection: 'flex-end'
    justifyContent: 'center',
    alignItems:'center',
    marginLeft: 250,
    marginBottom: 250
  },
  signout:{
    justifyContent: 'center',
    alignItems:'center',
    marginTop:80
  }
    

  })

export default userScreen