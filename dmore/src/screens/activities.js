import React, { Component } from 'react'
import { View, Text, Button,TextInput, StyleSheet } from 'react-native'
import { Formik, } from 'formik'

import firestore from '@react-native-firebase/firestore'

class activities extends Component{
    state = {
        activities: []
    }
    constructor(props){
        super(props);
        this.subscriber =
            firestore()
            .collection("Activities")
            .onSnapshot(docs => {
                let activities = []
                docs.forEach(doc => {
                    activities.push(doc.data())
                })
                this.setState({ activities })
                console.log(activities)
        })

    }

    addActivity = async () => {
        firestore()
        .collection('users/' + Uid)
        .add({
            category: this.state.category,
            name: this.state.name,
            info: this.state.info,
            req: this.state.req,
            size: this.state.size

        })
        .then(() => {
            console.log('activity Added!');
        });

        getActivities = async () => {
            firestore()
            .collection('activities')
            .onSnapshot(docs => {
                let activities = []
                docs.forEach(doc => {
                    activities.push(doc.data())
                })
                this.setState({ activities })
                console.log(activities)
        })
        }

        
        
        this.setState({ category: null, name: null, info:null,  req: null, size: null })
            }
            massDeleteActivities = async () => {
                const ActivitiesQuerySnapshot = await firestore()
                .collection('users')
                .get()

                const batch = firestore().batch();
                ActivitiesQuerySnapshot.forEach(documentSnapshot => {
                    batch.delete(documentSnapshot.ref);
                })
                return batch.commit();
            }
            
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                <Text style={styles.headertext}>Create an activity</Text>
                </View>
                <Formik
                    initialValues={{ category: "", name: "", info: "",req: "", size: "" }}
                    onSubmit={(values) => {

                    }}
                >
                    {(props) =>(
                        <View style={styles.inputcontainer}>
                        <TextInput
                                    style={styles.input}
                                    placeholder='Category'
                                    onChangeText={(category) => this.setState({ category })}
                                    value={this.state.category}
                                />
                        <TextInput
                                    style={styles.input}
                                    placeholder='Activity name'
                                    onChangeText={(name) => this.setState({ name })}
                                    value={this.state.name}
                                />
                        <TextInput
                                    style={styles.input}
                                    placeholder='Info'
                                    onChangeText={(info) => this.setState({ info })}
                                    value={this.state.info}
                                />
                        <TextInput
                                    style={styles.input}
                                    placeholder='Requirements'
                                    onChangeText={(req) => this.setState({ req })}
                                    value={this.state.req}
                                />
                        <TextInput
                                    style={styles.input}
                                    placeholder='Size'
                                    onChangeText={(size) => this.setState({ size })}
                                    value={this.state.size}
                                    keyboardType='numeric'
                                />
                        <Button title="Add an Activity" onPress={props.handleSubmit}/>
                        <Button title="Delete all activities" onPress={this.massDeleteActivities}/>
                        {this.state.activities.map((activity, index) => <View key={index}>
                        <Text>{activity.category}</Text>
                        </View>)}
                                </View>
                    )}
                
                </Formik>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        padding: 10,
    },
    header:{
        alignItems:'center',
        justifyContent: 'center',
        marginTop: 5,
    },
    headertext:{
        fontSize:20,
        fontFamily: 'serif',
    },
    inputcontainer:{
        flex:1,
        marginTop:10,

    },
    input:{
        marginTop: 10,
      borderWidth: 1,
      borderColor: '#ddd',
      padding: 10,
      fontSize: 18,
      borderRadius: 6,
    },
    category:{
        justifyContent:'center',
     
    },
    categorytext:{
        fontSize:18,
        paddingLeft:10,
        fontFamily: 'serif',
        marginTop: 10,
        borderBottomWidth:1,
        borderColor: 'black',
    
    },
    picker:{
        
    }

  })

export default activities