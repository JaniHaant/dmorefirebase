import React, { useState, useEffect  } from 'react';
import {View, StyleSheet, ImageBackground, Text, TouchableOpacity, Image} from 'react-native';
import Swiper from 'react-native-deck-swiper'
import firestore from '@react-native-firebase/firestore';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { FlatList } from 'react-native-gesture-handler';

const Card = ({card}) =>(
    <View style={styles.card}>
             <ImageBackground source={require('../components/images/GroupImg.jpg')} style={styles.cardImage} >
             <TouchableOpacity style={styles.avatarPlaceholder}></TouchableOpacity>
             <View style={styles.cardInfo}>
                <Text style={styles.cardtext}>{card}</Text>
             </View>
             <View  style={{ backgroundColor: '"rgba(93, 89, 89, 0.7)"', width: '100%', height: '20%'}}>
             <View style={{flexDirection: 'row',margin: 20, alignItems: 'center',justifyContent:'center', width:'90%',marginTop: 25}}>
             <TouchableOpacity style={{borderRadius: 45,backgroundColor:'crimson',marginLeft: 5,marginRight: 5,width: 120,padding: 10,alignItems: 'center'}}
                onPress={()=> swiperRef.current.swipeLeft()}><Text>PASS</Text></TouchableOpacity>
             <TouchableOpacity style={styles.button}
                onPress={()=> swiperRef.current.swipeRight()}><Text>ATTEND</Text></TouchableOpacity>
             </View> 
             </View>   
                </ImageBackground>      
        </View>
)

const swiperRef = React.createRef();

export default function activitycard(){ 
     
    const [activities, setActivities] = useState([])

        useEffect(() => {
            getData()         
    }, [activities] );
     
    console.log('toimiiko activities:', activities)

    const getData = async () => {
        firestore()
            .collection('activities')
            .onSnapshot(docs => {
                docs.forEach(doc => {
                    activities.push(doc.data())
                })   
                setActivities(activities)     
        })
    
    }
    
   const data  = activities.map((activity, index)=>     
        <View style={styles.cardcontent} key={index}>
            <View style={styles.cardheader}>
                <Text style={{fontSize:25, color:'black', fontWeight:'bold'}}>{activity.category}</Text>
            </View>
            <View style={styles.bottomcard}>
                <Text style={styles.cardtext}>{activity.category}</Text>
                <Text style={styles.cardtext}>{activity.info}</Text>
                <Text style={styles.cardtext}>{activity.req}</Text>
                <Text style={styles.cardtext}>{activity.size}</Text>
            </View>
        </View>  
   );
   
    const [index, setIndex,] = React.useState(0);
    
    const onSwiped = () => {
        setIndex((index + 1) % data.length)
    }
    const colors = {
        red: '#ec2379',
        green: '#008000',
        blue: '#0070ff',
        gray: '#777777',
        black: '#000000',
        white: '#ffffff'
    }  
    console.log('tää on data',data)
    return(
        <View style={styles.container}>
            
             <Swiper
                ref={swiperRef}
                key={index}
                cards={data}
                cardIndex={index}
                renderCard={(card) => <Card card={card} />}
                onSwiped={onSwiped}
                stackSize={4}
                stackScale={10}
                stackSeparation={14}
                disableTopSwipe
                disableBottomSwipe
                overlayLabels={{
                    left: {
                        title: 'PASS',
                        style:{
                            label:{
                                backgroundColor: colors.red,
                                color: colors.white,
                                fontSize: 24
                            },
                            wrapper:{
                                flexDirection: 'column',
                                alignItems: 'flex-end',
                                marginTop: 20,
                                marginLeft: -20
                            }
                        }
                    },
                    right: {
                        title: 'ATTEND',
                        style:{
                            label:{
                                backgroundColor: colors.green,
                                color: colors.white,
                                fontSize: 24
                            },
                            wrapper:{
                                flexDirection: 'column',
                                alignItems: 'flex-start',
                                marginTop: 20,
                                marginLeft: 20
                            }
                        }
                    }
                }}
                />   
                </View>  
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        },
    card:{
        flex:1,
        borderRadius:8,
        shadowRadius: 25,
        shadowColor:'#000',
        shadowOpacity: 0.08,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',    
        
            },
    cardImage:{
        flex: 1,
        resizeMode: 'cover',  
        width: '100%',
        height:'100%',
        marginTop:8,
            },
    avatarPlaceholder:{
                width: 50,
                height:50,
                marginLeft: 10,
                backgroundColor: 'black',
                borderRadius: 100,
                },
    cardInfo:{
        // flex: 1,
        marginTop: 190,
        width: '100%',
        alignItems: 'center',
        backgroundColor: '"rgba(93, 89, 89, 0.7)"'
            }, 
    // cardheader:{
    //     justifyContent:'center',
    //     alignItems: 'center',
    //     marginBottom: 10
        
    // },
    // bottomcard:{
    //     flex:1,
    //     width: 320,
    //     justifyContent:'center',
    //     alignItems: 'center',
    //     backgroundColor: '"rgba(93, 89, 89, 0.7)"',
    //     marginTop: 230
    // },
    cardtext:{
        fontSize: 18,
        color: 'white',
        justifyContent:'center',
        alignItems:'center',
    },
    button:{
        borderRadius: 45,
        backgroundColor:'green',
        marginLeft: 5,
        marginRight: 5,
        width: 120,
        padding: 10,
        alignItems: 'center'
            }

})
