import React, { useState, useEffect } from 'react';
import {View, Text, Button,StyleSheet, ImageBackground } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import auth from '@react-native-firebase/auth';
import MainTabScreen from './mainTabScreen'

anonymousSignIn = () => {
    auth()
    .signInAnonymously()
    .then(() => {
      console.log('User signed in anonymously');
    })
    .catch(error => {
      if (error.code === 'auth/operation-not-allowed') {
        console.log('Enable anonymous in your firebase console.');
      }
  
      console.error(error);
    });
  }

function landingPage ({navigation}) {

    const [user, setUser] = useState();
    const [initializing, setInitializing] = useState(true);

    function onAuthStateChanged(user) {
        setUser(user);
        if (initializing) setInitializing(false);
    }
    useEffect(() => {
        const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
        return subscriber; // unsubscribe on unmount
    }, []);
      
    if (initializing) return null;
        if (!user) {
          return (
            <View style={styles.container}>
            <ImageBackground style={styles.backgroundimg}
                source={require('../components/images/landbg.jpeg')}>
                    <View style={styles.header}>
                        <Text style={styles.headertext}>Dmore</Text>
                    </View>
                    <View style={styles.buttoncontainer}>
                    <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("signup")}>
                        <Text style={{fontSize: 15, fontWeight: 'bold', color: 'white'}}>Sign Up</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("signin")}>
                        <Text style={{fontSize: 15, fontWeight: 'bold', color: 'white'}}>Log in</Text>
                    </TouchableOpacity>
                    </View>
            <View>
                <TouchableOpacity style={styles.btn} onPress={() => {anonymousSignIn()}}>
                    <Text style={{fontSize: 15, color: 'white', fontWeight: 'bold'}}>Continue as guest</Text>
                </TouchableOpacity>
            </View>
            </ImageBackground>   
        </View>
          )
          }
          return (
            <MainTabScreen/>
            
          );
} ;

const styles = StyleSheet.create({
    container:{
        flex: 1,
    },
    backgroundimg:{
      flex: 1,
      resizeMode: "cover",
      justifyContent: "center",
      alignItems: 'center' 
    },
    header:{
        alignItems:'center',    
    },
    headertext:{
        fontWeight: 'bold',
        fontSize:40,
        fontFamily: 'serif',
        color: '#34902C'
    },
    buttoncontainer:{
        marginTop:'90%',
    },
    login:{
      justifyContent:'center',
      alignItems:'center'
    },
    button:{
        backgroundColor: '"rgba(44,120,164, 0.9)"',
        padding: 10,
        width: 300,
        alignItems: 'center',
        marginBottom: 10,
        borderRadius: 10
    },
    btn:{
        backgroundColor: '"rgba(52,164,44, 0.9)"',
        padding: 10,
        width: 300,
        alignItems: 'center',
        marginBottom: 10,
        borderRadius: 10
    }
})

export default landingPage