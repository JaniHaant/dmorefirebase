import React, { useState, useEffect }  from 'react'
import { View, Text, Button,TextInput, StyleSheet } from 'react-native'
import { Formik, } from 'formik'
import auth, { firebase } from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore'
import * as yup from 'yup';


const activitySchema = yup.object({
    category: yup.string().required().min(5),
    activityname: yup.string().required().min(4),
    info: yup.string().required().min(8),
    req: yup.string().required().min(8),
    size: yup.string()
    .required()
    .test('is-num-2-10', 'Size must be a number 2-10', (val)=>{
        return parseInt(val) < 10 && parseInt(val) > 1;
    })
})

function addActivivites(){
    const [initializing, setInitializing] = useState(true);
    const [user, setUser] = useState();
    const [activity,setActivity] = useState([])
    
      function onAuthStateChanged(user) {
        setUser(user);
        if(initializing){ setInitializing(false);
        //   console.log('tkeddta', user); 
        }
          else{

          }     
      }

      useEffect(() => {
        const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
        return subscriber; // unsubscribe on unmount
      }, []);

      const addActivivites = async(values) =>{
          setActivity(()=>{
              firestore()
              .collection('activities' )
              .add({
                  id: user.uid,
                  category: values.category,
                  activityname: values.activityname,
                  info: values.info,
                  req: values.req,
                  size: values.size,
              })
              .then(() => {
                  console.log('activity Added!');
                  setActivity(activity)   
              });
          })
      }

      const massDeleteUsers = async () => {
        const UsersQuerySnapshot = await firestore()
        .collection('users')
        .get()

        const batch = firestore().batch();
        UsersQuerySnapshot.forEach(documentSnapshot => {
            batch.delete(documentSnapshot.ref);
        })
        return batch.commit();
    }
    // console.log('täs',addActivivites)

    return(
        <View style={styles.container}>
            <View style={styles.header}>
            <Text style={styles.headertext}>Create an activity</Text>
        </View>
            <Formik
                initialValues={{ category: "", activityname: "", info: "",req: "", size: "" }}
                validationSchema={activitySchema}
                onSubmit={(values, actions) => {
                    actions.resetForm();
                    addActivivites(values)
                }}
                >
                {(props) =>(
                <View style={styles.inputcontainer}>
                    <TextInput
                        style={styles.input}
                        placeholder='Category'      
                        value={props.values.category}     
                        onChangeText={ props.handleChange('category')} 
                        onBlur={props.handleBlur('category')}
                    />
                    <Text style={styles.error}>{props.touched.category && props.errors.category}</Text>
                    <TextInput
                        style={styles.input}
                        placeholder='Activity name'
                        value={props.values.activityname}    
                        onChangeText={props.handleChange('activityname')}  
                        onBlur={props.handleBlur('activityname')}         
                    />
                    <Text style={styles.error}>{props.touched.activityname && props.errors.activityname}</Text>
                    <TextInput
                        style={styles.input}
                        placeholder='Info'
                        value={props.values.info}     
                        onChangeText={props.handleChange('info')}
                        onBlur={props.handleBlur('info')}
                    />
                    <Text style={styles.error}>{props.touched.info && props.errors.info}</Text>
                    <TextInput
                        style={styles.input}
                        placeholder='Requirements'
                        value={props.values.req}  
                        onChangeText={props.handleChange('req')}  
                        onBlur={props.handleBlur('req')}                  
                    />
                    <Text style={styles.error}>{props.touched.req && props.errors.req}</Text>
                    <TextInput
                        style={styles.input}
                        placeholder='Size'
                        keyboardType='numeric'
                        value={props.values.size}
                        onChangeText={props.handleChange('size')} 
                        onBlur={props.handleBlur('size')}
                    />   
                    <Text style={styles.error}>{props.touched.size && props.errors.size}</Text> 
                <Button title="Add an Activity" onPress={props.handleSubmit}/>
                </View>
                    )}
            </Formik>
            <Button title="Delete all käyttäjät" onPress={massDeleteUsers}/>
        </View>
    )
    
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        padding: 10,
    },
    header:{
        alignItems:'center',
        justifyContent: 'center',
        marginTop: 5,
    },
    headertext:{
        fontSize:20,
        fontFamily: 'serif',
    },
    inputcontainer:{
        flex:1,
        marginTop:10,

    },
    input:{
        marginTop: 10,
      borderWidth: 1,
      borderColor: '#ddd',
      padding: 10,
      fontSize: 18,
      borderRadius: 6,
    },
    category:{
        justifyContent:'center',
     
    },
    categorytext:{
        fontSize:18,
        paddingLeft:10,
        fontFamily: 'serif',
        marginTop: 10,
        borderBottomWidth:1,
        borderColor: 'black',
    
    },
    error:{
       color: 'crimson',
       fontSize: 12,
       textAlign: 'center' ,
       marginTop: 3,
    }

  })

export default addActivivites