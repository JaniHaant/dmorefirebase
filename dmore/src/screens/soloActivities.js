import React, { Component } from 'react';
import {StyleSheet, Image, View,Text, ImageBackground,} from 'react-native';
import Swiper from 'react-native-deck-swiper'
import firestore from '@react-native-firebase/firestore'
import { TouchableOpacity } from 'react-native-gesture-handler';

const Card = ( activities ) => (

        <View style={styles.card}>
             <ImageBackground source={require('../components/images/GroupImg.jpg')} style={styles.cardImage} >
             <View style={styles.cardInfo}>
             <Text style={{fontSize:28, marginBottom:10, fontWeight:'bold', color: 'white'}}>Golfing and wine</Text>
             <Text style={{fontSize:18, marginBottom:10, fontWeight:'bold',color: 'white'}}>Renting golf club costs around 5e</Text>
             <Text style={{fontSize:18, marginBottom:10, fontWeight:'bold', color: 'white'}}>minimum people of 2</Text>
             </View>
             </ImageBackground>
             <View style={{flexDirection: 'row',margin: 20, alignItems: 'center',justifyContent:'center', width:'100%'}}>
             <TouchableOpacity style={{borderRadius: 45,backgroundColor:'red',marginLeft: 5,marginRight: 5,width: 120,padding: 25,alignItems: 'center'}}><Text>PASS</Text></TouchableOpacity>
             <TouchableOpacity style={styles.button}><Text>ATTEND</Text></TouchableOpacity>
             </View>
             <Text>{activities.info}</Text>
             
        </View>
    );

class soloActivities extends Component {
    state = {
        activities: []
    }
    constructor(props){
        super(props);
        this.subscriber =
            firestore()
            .collection("activities")
            .onSnapshot(docs => {
                let activities = []
                docs.forEach(doc => {
                    activities.push(doc.data())
                })
                this.setState({ activities })
                console.log(activities)
        })

    }

     
    render(){
        return(
            <View style={styles.container}>
                <Swiper
                    onSwiped={(cardIndex) => {console.log(cardIndex)}}
                    cards={this.state.activities}
                    renderCard={(data) => <Card card={data}/>}
                    cardIndex={0}
                    stackSize= {4}
                />
            </View>
           
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor:'black',
        },
    card:{
        flex:1,
        borderRadius:8,
        shadowRadius: 25,
        shadowColor:'#000',
        shadowOpacity: 0.08,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',    
            },
    cardImage:{
        flex: 1,
        resizeMode: 'cover',  
        width: '100%',
        height:'100%',
        marginTop:8
            },
    cardInfo:{
        flex: 1,
        marginTop: '70%',
        width: '100%',
        alignItems: 'center',
        // backgroundColor: 'black'
            }, 
    button:{
        borderRadius: 45,
        backgroundColor:'green',
        marginLeft: 5,
        marginRight: 5,
        width: 120,
        padding: 25,
        alignItems: 'center'
            }

})

  export default soloActivities