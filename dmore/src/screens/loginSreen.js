import React, { useState, useEffect, useContext } from 'react';
import { View, Text, Button, StyleSheet, ImageBackground,TouchableOpacity  } from 'react-native';
import auth from '@react-native-firebase/auth';
import { GoogleSignin } from '@react-native-community/google-signin';
import HomeScreen from './homeScreen';
import firestore from '@react-native-firebase/firestore';
import MainTabScreen from './mainTabScreen';

GoogleSignin.configure({
  webClientId: '166152840740-362nk2icpm3pqp755l6raok2c6321pvl.apps.googleusercontent.com',
});

async function onGoogleButtonPress() {
  // Get the users ID token
  const { idToken } = await GoogleSignin.signIn();
  console.log('this id',idToken)

  // Create a Google credential with the token
  const googleCredential = auth.GoogleAuthProvider.credential(idToken);
  
  // Sign-in the user with the credential
  return auth().signInWithCredential(googleCredential);

}

anonymousSignIn = () => {
  auth()
  .signInAnonymously()
  .then(() => {
    console.log('User signed in anonymously');
  })
  .catch(error => {
    if (error.code === 'auth/operation-not-allowed') {
      console.log('Enable anonymous in your firebase console.');
    }

    console.error(error);
  });
}

  //log Off

  logoff = () => {
    auth()
      .signOut()
      .then(() => console.log('User signed out!'));
  }

function loginScreen({navigation}) {

  // Set an initializing state whilst Firebase connects
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();

  // const collectUserid = async() => {
    
  //   useEffect((user)=>{
  //     setUser(user)
  //     await firestore()
  //         .collection('users')
  //         .add({
  //             idToken: user.uid,
              
  //       })
  //       .then(() => {
  //         console.log('user id added', idToken  );
  //     });
  //   })
  // }

  // Handle user state changes
  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) setInitializing(false);

  }

  // function collectUserId(user){
  //   setUser(user)
  //   firestore()
  //     .collection('users')
  //     .add({
  //         idToken: user.uid,
          
  //   })
  //   .then(() => {
  //     console.log('user id added', idToken  );
  // });
    
  // }


  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount  
  }, []);

  if (initializing) return null;

  if (!user) {
    return (
      <View style={styles.container}>
        <ImageBackground style={styles.backgroundimg} source={require('../components/images/GroupImg.jpg')}>
          <View style={styles.header}>
            <Text>DoMore Find More to do</Text>
          </View>
          <View style={styles.footer}>
            <Button style={{width: '50%'}}
                    title="Google Sign-In"
                    onPress={() => { onGoogleButtonPress().then(() => console.log('Signed in with Google!'))}}
                    />
            <Button  
              style={{width: '50%'}}
              title="Sign-In anonoysmly"
              onPress={() => {anonymousSignIn()}}
            />
          </View>
        </ImageBackground>
      </View>
    );
  }

  return (
    <MainTabScreen/>
    
  );
}


export default loginScreen;

const styles=StyleSheet.create({
  container:{
    flex:1
  },
  backgroundimg:{
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
  header:{
    justifyContent: 'center',
    alignItems: 'center'
  },
  footer:{
    justifyContent:'center',
    alignItems:'center',
    width:'100%'
  },
  search:{
    flex: 1,
    borderRadius: 5,
  },
  Button:{
    flex: 1,
    borderRadius: 15,
    backgroundColor: 'green',
    justifyContent: 'center',
    alignItems: 'center',
    width: 320,
    marginBottom: 5,
    marginTop: 2,
    backgroundColor: "transparent"
  },
  text:{
    marginTop:'40%',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#C0392B'
  },
})