import React from 'react';
import {View, Text, StyleSheet, Button, ImageBackground,TouchableOpacity, } from 'react-native';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

const homeScreen = ({ navigation }) => {
    return(
        <View style={styles.container}>
          <ImageBackground
              style={styles.backgroundimg}
              source={require('../components/images/homebg.jpeg')}>
          <View style={styles.cover}>
            <View style={styles.header}> 
              <View style={styles.headtxt}>
              <Text style={{fontSize:18}}>Dmore</Text>
              </View>
            </View>
           <View style={styles.itemContainer}>
           <View style={styles.search}>
                <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("activitycard")}>
                  <Icon style={styles.icon} name="account-search-outline"  size={60} color={'green'} />
                  <Text style={styles.text}> Group Activities </Text>  
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("user")}>
                  <Icon  style={styles.icon} name="magnify"  size={60} color={'yellow'} />
                  <Text style={styles.text}> Activity</Text>  
                </TouchableOpacity>
            </View>
            <View style={styles.search}>
                <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("activities")}>
                  <Icon  style={styles.icon} name="plus-circle-outline"  size={60} color={'blue'} />
                  <Text style={styles.text}> Create An Activity </Text>  
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("search")}>
                  <Icon style={styles.icon} name="information-outline" size={60} color={'red'} />
                  <Text style={styles.text}> Your Activities</Text>  
                </TouchableOpacity>
            </View>
            </View>
            </View>
          </ImageBackground>
        </View>
    );
} ;

const styles=StyleSheet.create({
    container:{
      flex:1,   
    },
    backgroundimg:{
      flex: 1,
      resizeMode: "cover",
      justifyContent: "center",
      alignItems: 'center' ,
    },
    headtxt:{
      position:'absolute',
      left:     -150,
      top:      -20,
    },
    cover:{
      height: '100%',
      width: '100%',
      backgroundColor: '"rgba(93, 89, 89, 0.7)"',
      alignItems: 'center',
      justifyContent:'center',
    },
    itemContainer:{
      marginTop: 110
    },
    search:{
      // flex:1,
      justifyContent:'center',
      alignItems:'center',
      flexDirection:'row',
      marginTop:10
      // backgroundColor: '"rgba(93, 89, 89, 0.7)"', 
    },
    button:{
      width: 150,
      height: 150,
      flexDirection:'column',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '"rgba(85, 177, 220, 0.5)"',
      marginLeft: 5,
      marginRight: 5,
        
    },
    text:{
        color: '#F8F1F1',
        alignItems: 'center',
        justifyContent:'center',
    },
  })


export default homeScreen