import React, { Component } from 'react'
import { View, Text, Button,TextInput, StyleSheet } from 'react-native'
import { Formik, } from 'formik'

import firestore from '@react-native-firebase/firestore'

class editprofileScreen extends Component{
    state = {
        userinfo: []
    }
    constructor(props){
        super(props);
        this.subscriber =
            firestore()
            .collection("userinfo")
            .onSnapshot(docs => {
                let userinfo = []
                docs.forEach(doc => {
                    userinfo.push(doc.data())
                })
                this.setState({ userinfo })
                console.log(userinfo)
        })

    }

    addUserinfo = async () => {
        firestore()
        .collection('userinfo')
        .add({
            name: this.state.name,
            bio: this.state.bio,
            age: this.state.age,
            nationality: this.state.nationality,

        })
        .then(() => {
            console.log('User info updated!');
        });
        
        this.setState({ name: null, bio: null, age:null,  nationality: null })
            }
            
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                <Text style={styles.headertext}>Update user info</Text>
                </View>
                <Formik
                    initialValues={{ name: "", bio: "", age: "",nationality: "" }}
                    onSubmit={(values) => {

                    }}
                >
                    {(props) =>(
                        <View style={styles.inputcontainer}>
                        <TextInput
                                    style={styles.input}
                                    placeholder='Name'
                                    onChangeText={(name) => this.setState({ name })}
                                    value={this.state.name}
                                />
                        <TextInput
                                    style={styles.input}
                                    placeholder='bio'
                                    onChangeText={(bio) => this.setState({ bio })}
                                    value={this.state.bio}
                                />
                        <TextInput
                                    style={styles.input}
                                    placeholder='age'
                                    onChangeText={(age) => this.setState({ age })}
                                    value={this.state.age}
                                    keyboardType='numeric'
                                />
                        <TextInput
                                    style={styles.input}
                                    placeholder='nationality'
                                    onChangeText={(nationality) => this.setState({ nationality })}
                                    value={this.state.nationality}
                                />
                        <Button title="Submit update" onPress={this.addUserinfo}/>
                        {this.state.userinfo.map((userinfo, index) => <View key={index}>
                        <Text>{userinfo.category}</Text>
                        </View>)}
                                </View>
                    )}
                
                </Formik>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        padding: 10,
    },
    header:{
        alignItems:'center',
        justifyContent: 'center',
        marginTop: 5,
    },
    headertext:{
        fontSize:20,
        fontFamily: 'serif',
    },
    inputcontainer:{
        flex:1,
        marginTop:10,

    },
    input:{
        marginTop: 10,
      borderWidth: 1,
      borderColor: '#ddd',
      padding: 10,
      fontSize: 18,
      borderRadius: 6,
    },
    category:{
        justifyContent:'center',
     
    },
    categorytext:{
        fontSize:18,
        paddingLeft:10,
        fontFamily: 'serif',
        marginTop: 10,
        borderBottomWidth:1,
        borderColor: 'black',
    
    },
    picker:{
        
    }

  })

export default editprofileScreen