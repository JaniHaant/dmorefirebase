import React, { useState, useEffect } from 'react';
import {View, Text, StyleSheet, Image, ImageBackground } from 'react-native';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { Divider } from 'react-native-paper';
import auth from '@react-native-firebase/auth';
import LoginScreen from './loginSreen'
import firestore from '@react-native-firebase/firestore';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';

logoff = () => {
    auth()
      .signOut()
      .then(() => console.log('User signed out!'))
  }

function profileScreen ({navigation}) {
 
    const [initializing, setInitializing] = useState(true);
    const [user, setUser] = useState();
  
    // Handle user state changes
    function onAuthStateChanged(user) {
      setUser(user);
      if (initializing) setInitializing(false);
    }

    useEffect(() => {
      const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
      return subscriber; // unsubscribe on unmount
    }, []);
    
    if (initializing) return null;
  
    if (!user) {
      return (
        <LoginScreen/>
      );
    }

    return(
      
        <View style={styles.container}>
          <View style={styles.itemcontainer}>       
          <ImageBackground
              style={styles.backgroundimg}
              source={require('../components/images/profilebg.jpeg')}>
              <View style={styles.cover}>
                <TouchableOpacity style={styles.icon} onPress={() => {logoff()}}>
                  <Text style={{fontWeight:'bold', color:'#ffff'}}>Log Out</Text>
                  <Icon name="logout" style={{color: '#34902C',}} size={30} />
                </TouchableOpacity>
                <Image source={{uri: user.photo}} style={styles.avatarPlaceholder}></Image>
                <View style={styles.userinfo}>
                <Text style={{fontSize: 20, color: 'white'}}>{user.email}</Text>
                </View>
              </View>
          </ImageBackground>
          <View style={styles.infocontainer}>
            <View style={styles.information}>
              <Text style={{fontSize:24, fontWeight: 'bold'}}>Bio</Text>
              <Text style={{marginLeft:20}}>Tässä on paljon tekstiä joten alkaa tila loppumaanm,Tässä on paljon tekstiä joten alkaa tila loppumaanm</Text>
            </View>
            <Divider style={{color: 'blue', marginTop: 15, marginRight: 10, marginLeft: 10, borderWidth: 0.3}} />
            <View style={styles.information}>
              <Text style={{fontSize:24, fontWeight: 'bold'}}>Nationality</Text>
              <Text style={{marginLeft:20 }}>{user.email}</Text>
            </View>
            <Divider style={{color: 'blue', marginTop: 15, marginRight: 10, marginLeft: 10, borderWidth: 0.3 }} />
            <TouchableOpacity style={styles.buttons} onPress={() => navigation.navigate("editprofile")}>
              <Text>Edit Profile</Text>
              <Icon name="cog-outline" style={{color: 'grey',}} size={30} />
            {/* <Button
                title="Edit Profile"
                onPress={() => {logoff()}}/> */}
                </TouchableOpacity>
          </View>
          </View>  
        </View>
    );
} ;

const styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor: 'black'
    },
    itemcontainer:{
      flex:1
    },
    top:{
      justifyContent: 'center',
      alignItems:'center'  
    },
    backgroundimg:{
      flex: 0.8,
      resizeMode: "cover",
      justifyContent: "center",
      alignItems: 'center' ,
    },
    cover:{
      backgroundColor: '"rgba(93, 89, 89, 0.7)"',
      width: '100%',
      height: '100%',
      alignItems:'center',
      justifyContent: 'center'
    },
    icon:{
      flexDirection: 'row',
      justifyContent: 'flex-end' ,
      alignItems:'center',
      marginTop:5,
      marginLeft: 280,
      // backgroundColor:'green'
    },
    avatarPlaceholder:{
    width: 150,
    height:150,
    backgroundColor: '#E1E2E6',
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    },
    userinfo:{
      // marginTop: 10,
      marginLeft: 20,
    },
    infocontainer:{
      backgroundColor:'white',
      flex:1,
      width:'100%',
      borderRadius: 5
    },
    information:{
      marginLeft: 20,
      marginTop: 30,
      // backgroundColor:'green' 
    },
  ProfileText:{
    marginTop:55,
    marginLeft: 40,
    fontSize: 24
  },
  buttons:{
    marginTop: 10,
    // flexDirection: 'flex-end'
    justifyContent: 'center',
    alignItems:'center',
    marginLeft: 250,
    marginBottom: 250
  },
  signout:{
    justifyContent: 'center',
    alignItems:'center',
    marginTop:80
  }
    

  })

export default profileScreen