import React, { useState, useEffect  } from 'react';
import {View, StyleSheet, ImageBackground, Text, TouchableOpacity, ScrollView, Image,Card} from 'react-native';
import Swiper from 'react-native-deck-swiper'
import firestore from '@react-native-firebase/firestore';
import Postcard from '../components/Postcard'

// const Card = (card) =>{ 
//     return( 
//      <View style={styles.card}key={card.id}>
//                  <ScrollView>
//              <ImageBackground source={require('../components/images/GroupImg.jpg')} style={styles.cardImage} >
//              <View style={styles.cardInfo}>
//                 <Text style={styles.cardtext}>{card.activityname}</Text>
//                 <Text>budumzz</Text>
//                 <Text>töttöröö</Text>
//              </View>
//              <View  style={{ backgroundColor: '"rgba(93, 89, 89, 0.7)"', width: '100%', height: '20%'}}>
//              <View style={{flexDirection: 'row',margin: 20, alignItems: 'center',justifyContent:'center', width:'90%',marginTop: 25}}>
//              <TouchableOpacity style={{borderRadius: 45,backgroundColor:'crimson',marginLeft: 5,marginRight: 5,width: 120,padding: 10,alignItems: 'center'}}
//                 onPress={()=> swiperRef.current.swipeLeft()}><Text>PASS</Text></TouchableOpacity>
//              <TouchableOpacity style={styles.button}
//                 onPress={()=> swiperRef.current.swipeRight()}><Text>ATTEND</Text></TouchableOpacity>
//              </View> 
//              </View>
//                 </ImageBackground>      
//              <View style={{flex:1}}>
//                  <Image  style={{flex:1, width:100, resizeMode:'contain'}}source={require('../components/images/koira.jpeg')}/>
//              </View>
//              </ScrollView>
//         </View>
// )}

const swiperRef = React.createRef();

export default function activityContent(){ 
     
    const [posts, setPosts] = useState([])
    const [loading, setLoading ] = useState(true)

    
    useEffect(() => {
        const fetchPosts = async() =>{
            try {
                const list = [];
                await firestore()
                .collection('activities')
                .get()
                .then((querySnapshot) => {
                    querySnapshot.forEach(doc =>{
                        const { activityname, category, info, req, size } = doc.
                        data();
                        list.push({
                        id: doc.id,
                        activityname: activityname,
                        category: category,
                        info: info,
                        req: req,
                        size: size,
                    });
                    })
                })
                setPosts(list);

                if(loading){setLoading(false)}
                console.log('list on tässä', list)
            } catch(e) {
                console.log(e);
            }
            } 
            fetchPosts()
    }, [setPosts])
    
    const [index, setIndex,] = React.useState(0);
    
    const onSwiped = () => {
        setIndex((index + 1) % posts.length)
    }
    const colors = {
        red: '#ec2379',
        green: '#008000',
        blue: '#0070ff',
        gray: '#777777',
        black: '#000000',
        white: '#ffffff'
    }  
    
    console.log('postaukset', posts)
    return(
        <View style={styles.container}>
             <Swiper
                ref={swiperRef}
                key={index}
                cards={posts}
                // keyExtractor={card => card.id}
                cardIndex={index}
                renderCard={(card) => <Postcard card={card} />}
                // keyExtractor={card => card}
                onSwiped={onSwiped}
                stackSize={4}
                stackScale={10}
                stackSeparation={14}
                disableTopSwipe
                backgroundColor={'#F0F0F0'}
                disableBottomSwipe
                overlayLabels={{
                    left: {
                        title: 'PASS',
                        style:{
                            label:{
                                backgroundColor: colors.red,
                                color: colors.white,
                                fontSize: 24
                            },
                            wrapper:{
                                flexDirection: 'column',
                                alignItems: 'flex-end',
                                marginTop: 20,
                                marginLeft: -20
                            }
                        }
                    },
                    right: {
                        title: 'ATTEND',
                        style:{
                            label:{
                                backgroundColor: colors.green,
                                color: colors.white,
                                fontSize: 24
                            },
                            wrapper:{
                                flexDirection: 'column',
                                alignItems: 'flex-start',
                                marginTop: 20,
                                marginLeft: 20
                            }
                        }
                    }
                }}
                />   
                </View>  
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        },
    card:{
        flex:1,
        borderRadius:8,
        shadowRadius: 25,
        shadowColor:'#000',
        shadowOpacity: 0.08,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',    
        
            },
    cardImage:{
        flex: 1,
        resizeMode: 'cover',  
        width: '100%',
        height:'100%',
        marginTop:8,
            },
    cardInfo:{
        // flex: 1,
        marginTop: 120,
        width: '100%',
        alignItems: 'center',
        backgroundColor: '"rgba(93, 89, 89, 0.7)"'
            }, 
    cardtext:{
        fontSize: 18,
        color: 'white',
        justifyContent:'center',
        alignItems:'center',
    },
    button:{
        borderRadius: 45,
        backgroundColor:'green',
        marginLeft: 5,
        marginRight: 5,
        width: 120,
        padding: 10,
        alignItems: 'center'
            }

})
